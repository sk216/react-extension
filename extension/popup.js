// Copyright 2017 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.
'use strict';

var port = chrome.runtime.connect({name: "knockknock"});
//console.log(port);

port.postMessage({joke: "Knock knock"});

port.onMessage.addListener(function(msg) {
  //console.log(msg);
  let url = msg.url;
  let email = msg.email;
  let os  = msg.os;
  let reputation = msg.rep;
  console.log(url);
  console.log(email);
  console.log(os);

 let htmlcontent =  `
  <ul class="list-group">
    <li>url <span class="badge">${url}</span> <span class="badge"> ${reputation} </span> </li>
    <li>os <span class="badge">${os}</span></li>
  </ul>
  `

  var content = document.getElementById('content');
  content.innerHTML = htmlcontent

  //  chrome.action.openPopup();
  fetch('http://localhost:5000/upload/stats', {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({a: email, b: url,c:os})
  }).then(res => {
    console.log(res.status);
  })
  //  alert(url);

});

