## Chrome Extension using React

Read this article to build your own Chrome Extension using react:

_ run python server 

cd httpserver
python3 server.py 

Load extension in Browser 
visit a website and click on extension 

![](react-chrome-app/public/example.jpg)


[How To Build A Chrome Extension Using React](https://web-highlights.com/blog/how-to-build-a-chrome-extension-using-react/).

