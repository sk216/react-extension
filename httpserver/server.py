from flask import Flask, request, jsonify
#from pymongo import MongoClient

app = Flask(__name__)

#client = MongoClient('mongodb://localhost:27017/')
#db = client['ubadb']  # Replace 'myatabase' with your preferred database name
#collection = db['urls']  # Replac


@app.route('/upload/stats', methods=['GET','POST'])
def get_resource():
    # Your API logic here
    data = {"message": "This is a sample resource."}
    data = request.get_json()
    #db_id = collection.insert_one(data)
    print(data)
    response = {
            'message': 'Data successfully added to MongoDB',
            'inserted_id': "1" #db_id
    }
    return jsonify(response), 200



@app.route('/reputation', methods=['POST'])
def url_reputation():
    # Get the URL from the request data
    data = request.get_json()
    print(data)
    url = data.get('val')
    print(url)
    reputation = "clean"
    return jsonify({'url': url, 'reputation': reputation}),200





if __name__ == '__main__':
    app.run(debug=True)

