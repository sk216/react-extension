import React, {useState} from 'react';
import axios from 'axios';


function App() {
     const [currentFile, setCurrentFile] = useState<File>();
    function handleChange(event:any) {
      setCurrentFile(event.target.files[0])
    }

function handleSubmit(event:any) {
     event.preventDefault()
     const url = 'http://localhost:8000/uploadFile';
     const formData:any = new FormData();
     formData.append('file',currentFile);
     const config = {
       headers: {
         'content-type': 'multipart/form-data',
       },
     };
     axios.post(url, formData, config).then((response) => {
       console.log(response.data);
   });

  }
  return (
    <div>
      <header>
        <h2>Sample App</h2>
      </header>
      <div className="App">
              <form onSubmit={handleSubmit}>
                <h1>React File Upload</h1>
                <input type="file" onChange={handleChange}/>
                <button type="submit">Upload</button>
              </form>
          </div>


    </div>
  );
}

export default App;
