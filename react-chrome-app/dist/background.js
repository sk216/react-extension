/******/ (function() { // webpackBootstrap
var __webpack_exports__ = {};
/*!**************************************!*\
  !*** ./src/background/background.js ***!
  \**************************************/
chrome.tabs.onUpdated.addListener(() => {
  console.log('onUPdated sending url now...');
  // create alarm after extension is installed / upgraded
  logurl();
});

chrome.runtime.onConnect.addListener(function(port) {
  console.assert(port.name === "knockknock");
  port.onMessage.addListener(function(msg) {
    if (msg.joke === "Knock knock")
      port.postMessage({question: "Who's there?"});
    else if (msg.answer === "Madame")
      port.postMessage({question: "Madame who?"});
    else if (msg.answer === "Madame... Bovary")
      port.postMessage({question: "I don't get it."});
  });
});



function logurl(){
chrome.tabs.query({active: true, lastFocusedWindow: true}, tabs => {
   let url = tabs[0].url;
  // use `url` here inside the callback because it's asynchronous!
  // send url to remote endpoint
  if (url.startsWith('http') ) {

     console.log(url);
     fetch('https://httpbin.org/post/', {
       method: 'POST',
       headers: {
         'Accept': 'application/json',
         'Content-Type': 'application/json'
       },
       body: JSON.stringify({a: 1, b: url})
     }).then(res => {
       console.log(res.status);
     })
   }
});
}

/******/ })()
;
//# sourceMappingURL=background.js.map